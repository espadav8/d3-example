var path = require('path')

module.exports = {
  devtool: 'inline-source-map',
  entry: [
    './index.js'
  ],
  output: {
    path: path.resolve(__dirname, '.'),
    filename: 'bundle.js'
  },
  module: {
    loaders: [
      {
        test: /\.json$/,
        loader: 'json'
      },
      {
        test: /\.scss$/,
        loaders: ['style', 'css', 'sass']
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader'
      }
    ]
  },
  resolve: {
    alias: {
      'crossfilter': path.resolve(__dirname, 'node_modules/crossfilter2/')
    },
    extensions: ['', '.js']
  }
}
